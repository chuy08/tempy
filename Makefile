include .project/gomod-project.mk

BIN=tempy
SRC=tempy

BINDIR=bin

# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build

all: tools build

tools:
	go install golang.org/x/tools/cmd/stringer@latest
	go install golang.org/x/tools/cmd/godoc@latest
	go install golang.org/x/tools/cmd/guru@latest
	go install golang.org/x/lint/golint@latest

vendor:
	go mod vendor

build: config
	$(GOBUILD) ${BUILD_FLAGS} -o $(BINDIR)/$(BIN) main.go

build-arm: config
	GOOS=linux GOARCH=arm64 $(GOBUILD) ${BUILD_FLAGS} -o $(BINDIR)/$(BIN)_arm64 main.go
	scp -pr bin/tempy_arm64 raspberrypi:.
	scp -pr public raspberrypi:.

clean:
	-rm bin/*

config:
	mkdir -p $(BINDIR)

# Verify that there are no symlinks in our /vendor paths. Vendor'd directories sometime
# bring in symlinks that are recursive. This command will error out if 'find' encounters a recursive symlink
verify-symlinks:
	@find -L . -printf ""

.PHONY: clean test config