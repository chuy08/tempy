package config

import (
	"encoding/json"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

const (
	defaultConfigFilename = "config"
	envPrefix             = "TEMPY"
)

// TimeSeries data as generated by our thermometer.
type TimeSeries struct {
	Name string
	T    float64
	TS   int64
}

// Server configures the echo server.
type Server struct {
	Env           string `mapstructure:"env"`
	File          string `mapstructure:"file"`
	FlushInterval int    `mapstructure:"flush_interval"`
	HideBanner    bool   `mapstructure:"hide_banner"`
	Port          int    `mapstructure:"port"`
}

// DBConfig Sqlite3 configuration.
type DBConfig struct {
	FileName string `mapstructure:"file_name"`
}

// ViperConfig configuration.
type ViperConfig struct {
	vConfig *viper.Viper
}

// LoggerConfig configuration.
type LoggerConfig struct {
	DisableStacktrace bool   `mapstructure:"disable_stack_trace"`
	Encoding          string `mapstructure:"encoding"`
	Level             string `mapstructure:"level"`
}

func New() (*ViperConfig, error) {
	v := viper.New()

	// Set the base name of the config file, without the file extension.
	v.SetConfigName(defaultConfigFilename)

	// Set as many paths as you like where viper should look for the
	// config file. We are only looking in the current working directory.
	v.AddConfigPath(".")

	// Attempt to read the config file, gracefully ignoring errors
	// caused by a config file not being found. Return an error
	// if we cannot parse the config file.
	if err := v.ReadInConfig(); err != nil {
		// It's okay if there isn't a config file
		// !errors.Is(err, syscall.ENOTTY)
		// trunk-ignore(golangci-lint/errorlint)
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return nil, err
		}
	}

	// When we bind flags to environment variables expect that the
	// environment variables are prefixed, e.g. a flag like --number
	// binds to an environment variable STING_NUMBER. This helps
	// avoid conflicts.
	v.SetEnvPrefix(envPrefix)

	// Environment variables can't have dashes in them, so bind them to their equivalent
	// keys with underscores, e.g. --favorite-color to STING_FAVORITE_COLOR
	v.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))

	// Bind to environment variables
	// Works great for simple config names, but needs help for names
	// like --favorite-color which we fix in the bindFlags function
	v.AutomaticEnv()

	// Bind the current command's flags to viper
	// bindFlags(cmd, v)

	c := &ViperConfig{
		vConfig: v,
	}

	return c, nil
}

// LogConfig loads our logger configuration information.
func (c *ViperConfig) GetLoggerConfig() (*LoggerConfig, error) {
	var cfg *LoggerConfig
	zap := c.vConfig.Sub("logger")
	err := zap.Unmarshal(&cfg)
	if err != nil {
		return nil, errors.Wrapf(err, "logger unmarshal failure")
	}
	// cfg.Level = c.vConfig.GetString("level")
	return cfg, nil
}

// GetSqlite3Config returns Psql config.
func (c *ViperConfig) GetSqlite3Config() (*DBConfig, error) {
	var p *DBConfig
	db := c.vConfig.Sub("db")
	err := db.Unmarshal(&p)
	if err != nil {
		return nil, errors.Wrap(err, "sqlite3 unmarshal failure")
	}
	return p, nil
}

// GetServerConfig echo server config.
func (c *ViperConfig) GetServerConfig() (*Server, error) {
	var s *Server
	server := c.vConfig.Sub("server")
	err := server.Unmarshal(&s)
	if err != nil {
		return nil, errors.Wrap(err, "Server unmarshal failure")
	}
	return s, nil
}

// GetConfigsJSON returns all the active configurations used in json format.
func (c *ViperConfig) GetConfigsJSON() ([]byte, error) {
	configs := make(map[string]string)

	// Get all keys and values from viper
	ckeys := c.vConfig.AllKeys()
	for _, k := range ckeys {
		val := c.vConfig.GetString(k)
		if val != "" {
			configs[k] = val
		}
	}

	buf, err := json.Marshal(configs)
	if err != nil {
		return nil, errors.Wrapf(err, "error marshaling config")
	}

	return buf, nil
}

/* // cfgFileParts returns a map of file path, filename, and file extension
func cfgFileParts(cfgFile string) map[string]string {
	m := make(map[string]string)
	split := strings.Split(cfgFile, "/")
	l := len(split)
	if l <= 2 {
		i := l - 1
		m["path"] = "."
		m["fileName"] = split[i]
		m["extension"] = fileExt(split[i])
	} else {
		last := len(split) - 1
		p := split[0:last]
		m["path"] = strings.Join(p, "/")
		m["fileName"] = split[last]
		m["extension"] = fileExt(split[last])
	}
	return m
}

// fileExt returns the config file extension hopefully json or yaml :)
func fileExt(file string) string {
	s := strings.Split(file, ".")
	return s[len(s)-1]
}

// urlParts returns host, port, path, sslMode from url
func urlParts(url string) (string, string, string, bool) {
	u := parseURL(url)

	// PSQL database is the typical path, so we replace the leading slash
	// Rabbit MQ vhost - curently unused
	path := strings.Replace(u.Path, "/", "", -1)

	// Returns host & Port
	host, port, err := net.SplitHostPort(u.Host)
	if err != nil {
		errors.Wrapf(err, "Splitting host and port went wrong")
	}

	// returns true, bit is flipped (!)
	local := contains([]string{"127.0.0.1", "localhost"}, host)
	return host, port, path, local
}

// parseURL After sanitzing standard URL parsing
func parseURL(psqlURL string) *url.URL {
	u, err := url.Parse(psqlURL)
	if err != nil {
		errors.Wrapf(err, "This is not good")
	}
	return u
}

// contains string in array
func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
} */
