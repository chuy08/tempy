package logger

import (
	"tempy/config"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// InitLogger returns logger for a given log level.
func InitLogger(config *config.LoggerConfig) (*zap.Logger, error) {
	atom := zap.NewAtomicLevel()
	l, err := zapcore.ParseLevel(config.Level)
	if err != nil {
		return nil, errors.Wrapf(err, "Parselevel parse failure")
	}

	cfg := zap.Config{
		Development:       true,
		DisableCaller:     false,
		DisableStacktrace: config.DisableStacktrace,
		Encoding:          config.Encoding,
		EncoderConfig: zapcore.EncoderConfig{
			CallerKey:     "caller",
			LevelKey:      "level",
			MessageKey:    "msg",
			StacktraceKey: "stacktrace",
			TimeKey:       "ts",
			EncodeCaller:  zapcore.ShortCallerEncoder,
			EncodeLevel:   zapcore.LowercaseLevelEncoder,
			EncodeTime:    zapcore.ISO8601TimeEncoder,
		},
		ErrorOutputPaths: []string{"stderr"},
		Level:            atom,
		OutputPaths:      []string{"stdout"},
	}

	logger := zap.Must(cfg.Build())
	atom.SetLevel(l)

	return logger, nil
}
