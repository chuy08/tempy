/*
Copyright © 2023 Jesus Orosco chuy08@gmail.com
*/
package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"tempy/config"
	"tempy/internal/logger"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"go.uber.org/zap"
	"golang.org/x/net/websocket"
)

type Tempy struct {
	env   string
	file  string
	flush chan *config.TimeSeries
	log   *zap.Logger
}

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatalf("viper config failure: %v", err)
	}

	logConfig, err := cfg.GetLoggerConfig()
	if err != nil {
		log.Fatalf("Logger Config failure: %v", err)
	}

	logger, err := logger.InitLogger(logConfig)
	if err != nil {
		log.Fatalf("Logger init failure: %v", err)
	}

	defer func() {
		err = logger.Sync()
		if err != nil && !errors.Is(err, syscall.ENOTTY) {
			logger.Sugar().Errorf("Log Sync Failure: %v", err)
		}
	}()

	logger.Info("Logger initialized ")

	configJSON, err := cfg.GetConfigsJSON()
	if err != nil {
		logger.Sugar().Infof("Could not get configuration as JSON. Error = %v", err)
	} else {
		logger.Info("Starting with Config:", zap.Any("object", json.RawMessage(configJSON)))
	}
	logger.Sugar().Debug("debug logging enabled")

	/*
		// Using Sony Flake for Primary key, wondering if it's necessary
		pk := sonyflake.NewSonyflake(sonyflake.Settings{
			StartTime: time.Date(2022, 6, 8, 0, 0, 0, 0, time.UTC),
		})

		 	dbConfig, err := cfg.GetSqlite3Config()
		   	if err != nil {
		   		logger.Sugar().Errorf("Database config failure: %#v", err)
		   	}
		   	provider, err := db.New(dbConfig, pk, logger)
		   	if err != nil {
		   		logger.Sugar().Errorf("Database New Object failure: %#v", err)
		   	}

		   	defer provider.DB.Close()
	*/

	serverConfig, err := cfg.GetServerConfig()
	if err != nil {
		logger.Sugar().Errorf("Server Config error: %v", err)
	}

	// populating our Tempy structure
	a := Tempy{
		//db:   provider,
		env:  serverConfig.Env,
		file: serverConfig.File,
		// trunk-ignore(golangci-lint/gomnd)
		flush: make(chan *config.TimeSeries, 20),
		log:   logger,
	}

	// thread that will clear the channel and flush to sqlite3
	interval := time.Duration(serverConfig.FlushInterval) * time.Second
	logger.Sugar().Infof("flush every %d seconds", serverConfig.FlushInterval)

	// Background Context for flushing
	ctx := context.Background()

	// doEvery is a cron thread to perfom tasks
	go func() {
		err = doEvery(ctx, interval, a.flusher)
		if err != nil {
			logger.Sugar().Errorf("doEvery error: [%v]", err)
		}
	}()

	// Initiate echo server
	e := serverInit(logger, serverConfig)

	// Registering our routes and handlers
	e.File("/", "public/index.html")
	e.File("/two", "public/two.html")
	e.Static("/static", "public/assets")

	e.GET("/ws", a.socket)
	e.GET("/version", version)

	port := fmt.Sprintf(":%d", serverConfig.Port)
	h := http.Server{
		Addr:     port,
		Handler:  e,
		ErrorLog: zap.NewStdLog(logger),
		// trunk-ignore(golangci-lint/gomnd)
		ReadHeaderTimeout: time.Duration(10) * time.Second,
	}

	logger.Sugar().Infof("Server starting on port: %s", port)
	if err = h.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		logger.Sugar().Fatalf("Shutting down server: %s", err.Error())
	}
}

// flusher flushes timeseries to sqlite3.
func (a *Tempy) flusher(t time.Time) {
	l := len(a.flush)
	a.log.Sugar().Infow("Heart Beat", "length", l)

	var data []*config.TimeSeries
	if l > 0 {
		for l > 0 {
			data = append(data, <-a.flush)
			l--
		}
		//a.db.Insert(data)
	}
}

// socket handler for WebSockets back to our Javascript graph.
func (a *Tempy) socket(c echo.Context) error {
	websocket.Handler(func(ws *websocket.Conn) {
		defer ws.Close()
		for {
			ch := make(chan *config.TimeSeries)

			var wg sync.WaitGroup
			wg.Add(1)

			if strings.Contains(a.env, "pi") {
				// fmt.Printf("Hi Chuy from env: %v\n", a.env)
				go a.readFile(&wg, ch)
			} else {
				// TODO: replace with actual measurements
				go a.randomW1Data(&wg, ch, 10)
			}

			for {
				v, ok := <-ch
				if !ok {
					a.log.Sugar().Warnf("channel closed: [%v]", ok)
					break
				}
				// Write
				err := websocket.JSON.Send(ws, v)
				if err != nil {
					a.log.Sugar().Errorf("channel closed: [%v]", err)
					return
				}

				a.flush <- v
				a.log.Sugar().Debugf("channel length: %v", len(a.flush))
			}

			// (Unused) Read a response from the webserver
			msg := ""
			err := websocket.Message.Receive(ws, &msg)
			if err != nil {
				c.Logger().Error(err)
				return
			}
			wg.Wait()
		}
	}).ServeHTTP(c.Response(), c.Request())
	return nil
}

// doEvery runs a function (flusher) at a given flush_interval.
func doEvery(ctx context.Context, d time.Duration, f func(time.Time)) error {
	ticker := time.Tick(d)
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case x := <-ticker:
			f(x)
		}
	}
}

// readFile reads the file that contains the current temperature measurement.
func (a *Tempy) readFile(wg *sync.WaitGroup, ch chan *config.TimeSeries) {
	defer wg.Done()
	var previous float64
	for {
		dat, err := os.ReadFile(a.file)
		if err != nil {
			a.log.Sugar().Errorf("Read file error: %v", err)
		}
		a.log.Sugar().Debugf("string: %v, length: %v", string(dat), len(string(dat)))
		var fahrenheit float64
		if len(string(dat)) == 0 {
			a.log.Sugar().Debugf("No data found, using previous value: %v", fahrenheit)
			fahrenheit = previous
		} else {
			fahrenheit = a.parseMeasurement(string(dat))
			previous = fahrenheit
		}

		ts := &config.TimeSeries{
			Name: "ThermA",
			// trunk-ignore(golangci-lint/gomnd)
			T:  fahrenheit,
			TS: time.Now().UnixMilli(),
		}
		ch <- ts
		time.Sleep(time.Duration(2) * time.Second)
	}
}

// randomW1Data generates "real" random data.
func (a *Tempy) randomW1Data(wg *sync.WaitGroup, ch chan *config.TimeSeries, limit int) {
	defer wg.Done()
	// RandomData is hardcoded to a total of 1000 points
	for idx := 0; idx <= 1000; idx++ {
		var measure string
		if idx <= limit {
			// Random float64
			m := randFloats(float64(idx), float64(idx+1))
			// Rounding and Precsion of random Float
			t := toFixed(m, 3)
			// Convert to string
			tString := fmt.Sprintf("%.3f", t)
			// Strip the period like real data
			measure = strings.ReplaceAll(tString, ".", "")
			// fmt.Printf("min: %v - max: %v - m: %v - real: %v\n", idx, idx+1, m, measure)
		} else {
			// fmt.Printf("hit the limit boom\n")
			m := randFloats(float64(limit-1), float64(limit))
			t := toFixed(m, 3)
			tString := fmt.Sprintf("%.3f", t)
			measure = strings.ReplaceAll(tString, ".", "")
			// fmt.Printf("min: %v - max: %v - m: %v - real: %v\n", limit-1, limit, m, measure)
		}
		realData := fmt.Sprintf(`ec 01 55 05 7f a5 a5 66 3e : crc=3e YES\nec 01 55 05 7f a5 a5 66 3e t=%v\n\n`, measure)

		fmt.Printf("%v\n", realData)
		fahrenheit := a.parseMeasurement(realData)
		a.log.Sugar().Debugf("f: %v\n", fahrenheit)

		ts := &config.TimeSeries{
			Name: "ThermA",
			// trunk-ignore(golangci-lint/gomnd)
			T:  fahrenheit,
			TS: time.Now().UnixMilli(),
		}
		ch <- ts
		time.Sleep(time.Duration(2) * time.Second)
	}
}

func (a *Tempy) parseMeasurement(realData string) float64 {
	var fahrenheit float64
	if strings.Contains(realData, "t=") {
		splitStrings := strings.Split(realData, "t=")
		trim := strings.TrimRight(splitStrings[1], "\n")
		c, err := strconv.ParseFloat(trim, 64)
		if err != nil {
			a.log.Sugar().Errorw("parse error", "error", err)
		}
		celsius := toFixed(c*0.001, 3)
		f := (celsius * 1.8) + 32
		fahrenheit = toFixed(f, 3)
		a.log.Sugar().Debugw("temperature readings", "raw", splitStrings[1], "fahrenheit", fahrenheit, "celsius", celsius)
	}
	return fahrenheit
}

// randomFloats returns a random float64.
func randFloats(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

// version returns application version.
func version(c echo.Context) error {
	return c.JSON(http.StatusOK, "0.0.1")
}

// serverInit initializes a new Echo Server.
func serverInit(logger *zap.Logger, config *config.Server) *echo.Echo {
	e := echo.New()
	e.HideBanner = config.HideBanner

	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogURI:      true,
		LogStatus:   true,
		LogError:    true,
		LogMethod:   true,
		HandleError: true,
		LogValuesFunc: func(c echo.Context, v middleware.RequestLoggerValues) error {
			if v.Error == nil {
				logger.Info("request",
					zap.String("method", v.Method),
					zap.String("URI", v.URI),
					zap.Int("status", v.Status),
				)
			} else {
				logger.Error("request error",
					zap.String("method", v.Method),
					zap.String("URI", v.URI),
					zap.Int("status", v.Status))
			}
			return nil
		},
	}))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))
	return e
}
