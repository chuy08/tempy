package csv

import (
	"encoding/csv"
	"os"
)

type Foo struct {
	c *csv.Writer
}

func New() *Foo {
	//fmt.Println("From CSV")

	// Write the CSV data
	file2, err := os.Create("data1.csv")
	if err != nil {
		panic(err)
	}
	defer file2.Close()

	f := &Foo{
		c: csv.NewWriter(file2),
	}

	return f
	//writer := csv.NewWriter(file2)
	//defer writer.Flush()

	/*
		 	headers := []string{"TS", "Name", "Value"}

			data1 := [][]string{
				{"Alice", "25", "Female"},
				{"Bob", "30", "Male"},
				{"Charlie", "35", "Male"},
			}

			writer.Write(headers)
			for _, row := range data1 {
				writer.Write(row)
			}
	*/
}
